# Sample App
This was developed on a laptop running Debian Stable, so that comes
suggested, but a knowledgable developer should have no problem on
other platforms or distributions.

Services and software will be launched with `docker-compose` via
Python 2.7.

A Bash shell is highly suggested.
A Bash compatible shell (such as zsh) should work as well but is less
preferred.
You're on your own for other shells that are only partially Bash
compatible (looking at you PowerShell).

Windows will probably be problematic unless you're running this in
Linux using one of the recent Windows Creator's Updates that gives
you the "Windows Subsystem for Linux" and you then run the app using
that. You're on your own for setting any of this up on Windows.

# Getting Docker
[Install Docker per your platform instructions.](https://docs.docker.com/install/)

If you're using Linux, make sure that you also follow the appropriate
instructions to make yourself part of the correct group to run docker
since it does require root priveleges.

# Getting Python, `pip`, and `pipenv`
These are essential tools and packages for getting `docker-compose` going.
You should make sure that you have Python 2.7 installed, along with
the packages `pip` and `pipenv`.

**NOTE** - Do Not Install `pipenv` or `docker-compose`  via the system package
manager.
You might end up with older versions that will not support the current version of
docker-compose.

`pip` is often bundled with Python, so then you just have to run
`pip install pipenv` to get `pipenv` installed at the current version.

# Steps for getting and launching application

```bash
# Clone the repo.
git clone https://gitlab.com/jeremy.jackson/sample-app.git
cd sample-app

# Get the dependencies for docker-compose and docker-compose itself.
pipenv install

# Enter the virtualenv that docker-compose will run inside of.
pipenv shell

# Build the docker containers (this will install the node
# dependencies inside the containers).
# This may take some time so be patient.
docker-compose build

# Launch the application, silencing stdout from the containers.
docker-compose up -d

# You should now be able to launch a web browser and point it at
# http://localhost:3000 to view the web app.

# When you're done and want to tear things down.
# First bring down the docker containers.
docker-compose down

# Then exit the pipenv shell
exit
```
