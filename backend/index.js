/* eslint import/newline-after-import: "off" */
// Turning this off makes it easier to quickly comment out a route.

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

const router = express.Router();
router.get('/', (req, res) => {
  res.status(200).json({ exists: 1 });
});
app.use('/', router);

app.use('/get_user', require('./src/get_user.route.js'));
app.use('/create_new_user', require('./src/create_new_user.route.js'));
app.use('/current_points', require('./src/get_current_points.route.js'));
app.use('/new_checkin', require('./src/new_checkin.route.js'));
app.use('/can_checkin', require('./src/can_checkin.route.js'));

app.listen('3001');

module.exports = app;
