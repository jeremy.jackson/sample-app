const expect = require('expect.js');
const nano = require('nano')('http://couchdb:5984');

const getCurrentPoints = require('../src/get_current_points.db.js');

describe('backend/src/get_current_points.db.js', () => {
  describe('getCurrentPoints()()', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const doc = {
                _id: '1234567890',
                name: { first: 'bob', last: 'jones' },
                phone_number: '1234567890',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: (new Date()).toJSON(), points_earned: 50 },
                ],
              };
              const appUsers = nano.use('app_users');
              appUsers.insert(doc, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                done();
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const doc = {
              _id: '1234567890',
              name: { first: 'bob', last: 'jones' },
              phone_number: '1234567890',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: (new Date()).toJSON(), points_earned: 50 },
              ],
            };
            const appUsers = nano.use('app_users');
            appUsers.insert(doc, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              done();
            });
          });
        }
      });
    });

    it('should resolve with an object equal to reference when given a valid id', (done) => {
      getCurrentPoints('1234567890')
        .then((res) => {
          expect(res).to.be.eql({ current_points: 50 });
          done();
        })
        .catch(err => done(err));
    });
  });
});

