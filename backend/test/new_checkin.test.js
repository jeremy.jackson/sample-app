const nano = require('nano')('http://couchdb:5984');
const expect = require('expect.js');
const newCheckin = require('../src/new_checkin.db.js');

describe('backend/src/new_checkin.db.js', () => {
  describe('newCheckin()', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const doc1 = {
                _id: '1234567890',
                name: { first: 'bob', last: 'jones' },
                phone_number: '1234567890',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
                ],
              };
              const doc2 = {
                _id: '0987654321',
                name: { first: 'tammy', last: 'jones' },
                phone_number: '0987654321',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: (new Date()).toJSON(), points_earned: 50 },
                ],
              };
              const appUsers = nano.use('app_users');
              appUsers.insert(doc1, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                appUsers.insert(doc2, (insertErr2) => {
                  if (insertErr2) throw new Error(insertErr2);
                  done();
                });
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const doc1 = {
              _id: '1234567890',
              name: { first: 'bob', last: 'jones' },
              phone_number: '1234567890',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
              ],
            };
            const doc2 = {
              _id: '0987654321',
              name: { first: 'tammy', last: 'jones' },
              phone_number: '0987654321',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: (new Date()).toJSON(), points_earned: 50 },
              ],
            };
            const appUsers = nano.use('app_users');
            appUsers.insert(doc1, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              appUsers.insert(doc2, (insertErr2) => {
                if (insertErr2) throw new Error(insertErr2);
                done();
              });
            });
          });
        }
      });
    });

    it('should give a positive checkedin message', (done) => {
      newCheckin('1234567890')
        .then((res) => {
          expect(res.checkedin).to.be.ok();
          done();
        })
        .catch(err => done(err));
    });

    it('should give a failing checkedin message', (done) => {
      newCheckin('0987654321')
        .then((res) => {
          expect(res.checkedin).to.not.be.ok();
          done();
        })
        .catch(err => done(err));
    });
  });
});

