const expect = require('expect.js');

const checkFiveMinutes = require('../src/check_five_minutes.util.js');

describe('backend/src/check_five_minutes.util.js', () => {
  describe('checkFiveMinutes()', () => {
    it('should return true if difference between two times is > 5 minutes', () => {
      const trueTest = [
        ['2018-01-01T00:00:00.000Z', '2018-01-01T00:05:01.000Z'],
        ['2018-01-01T00:00:00.000Z', '2018-01-01T00:15:01.000Z'],
      ];

      trueTest.forEach((test) => {
        expect(checkFiveMinutes(test[0], test[1])).to.be.ok();
      });
    });

    it('should return false if difference between two times is <= 5 minutes', () => {
      const falseTest = [
        ['2018-01-01T00:00:00.000Z', '2018-01-01T00:05:00.000Z'],
        ['2018-01-01T00:00:00.000Z', '2018-01-01T00:04:59.000Z'],
      ];

      falseTest.forEach((test) => {
        expect(checkFiveMinutes(test[0], test[1])).to.not.be.ok();
      });
    });
  });
});
