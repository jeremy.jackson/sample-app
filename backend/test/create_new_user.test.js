const nano = require('nano')('http://couchdb:5984');
const expect = require('expect.js');
const createNewUser = require('../src/create_new_user.db.js');

describe('backend/src/create_new_user.db.js', () => {
  describe('createNewUser()', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              done();
            });
          });
        } else {
          nano.db.create('app_users', () => {
            done();
          });
        }
      });
    });

    it('should return the database success message when successfully inserted', (done) => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      createNewUser(test)
        .then((res) => {
          expect(res).to.have.keys('ok', 'id', 'rev');
          expect(res.ok).to.be.ok();
          done();
        })
        .catch(err => done(err));
    });

    it('should return an error message if the insert failed', (done) => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        // current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      createNewUser(test)
        .then((res) => {
          expect(res).to.eql({ malformed: true });
          done();
        })
        .catch(err => done(err));
    });
  });
});
