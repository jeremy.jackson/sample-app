const expect = require('expect.js');
const nano = require('nano')('http://couchdb:5984');

const getUser = require('../src/get_user.db.js');

describe('backend/src/get_user.db.js', () => {
  describe('getUser()', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const appUsers = nano.use('app_users');
              appUsers.insert({ _id: '1234567890', key: 'value' }, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                done();
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const appUsers = nano.use('app_users');
            appUsers.insert({ _id: '1234567890', key: 'value' }, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              done();
            });
          });
        }
      });
    });

    it('should resolve with a truthy value when given a valid id', (done) => {
      getUser('1234567890')
        .then((res) => {
          expect(res).to.be.ok();
          done();
        })
        .catch(err => done(err));
    });

    it('should resolve with a falsey value when given an invalid id', (done) => {
      getUser('0987654321')
        .then((res) => {
          expect(res).to.not.be.ok();
          done();
        })
        .catch(err => done(err));
    });
  });
});
