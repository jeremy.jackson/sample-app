const expect = require('expect.js');
const request = require('supertest');
const nano = require('nano')('http://couchdb:5984');

const app = require('../index.js');

describe('backend/index.js', () => {
  describe('GET /', () => {
    it('should return status code 200 with body equal to reference', (done) => {
      request(app)
        .get('/')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.eql({ exists: 1 });
          return done();
        });
    });
  });

  describe('GET /get_user/:id', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const appUsers = nano.use('app_users');
              appUsers.insert({ _id: '1234567890', key: 'value' }, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                done();
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const appUsers = nano.use('app_users');
            appUsers.insert({ _id: '1234567890', key: 'value' }, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              done();
            });
          });
        }
      });
    });

    it('should return status code 200 with body equal to reference when given valid id', (done) => {
      request(app)
        .get('/get_user/1234567890')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.eql({ exists: 1 });
          return done();
        });
    });

    it('should return status code 404 with body equal to reference when given invalid id', (done) => {
      request(app)
        .get('/get_user/0987654321')
        .expect('Content-Type', /json/)
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.eql({ exists: 0 });
          return done();
        });
    });
  });

  describe('POST /create_new_user', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              done();
            });
          });
        } else {
          nano.db.create('app_users', () => {
            done();
          });
        }
      });
    });

    it('should return status code 200 with body equal to reference when given valid id', (done) => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      request(app)
        .post('/create_new_user')
        .send(test)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.have.keys('ok', 'id', 'rev', 'malformed');
          return done();
        });
    });

    it('should return status code 400 with body equal to reference when given invalid id', (done) => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        // phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      request(app)
        .post('/create_new_user')
        .send(test)
        .expect('Content-Type', /json/)
        .expect(400)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.have.keys('malformed');
          return done();
        });
    });
  });

  describe('GET /current_points/:id', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const doc = {
                _id: '1234567890',
                name: { first: 'bob', last: 'jones' },
                phone_number: '1234567890',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: (new Date()).toJSON(), points_earned: 50 },
                ],
              };
              const appUsers = nano.use('app_users');
              appUsers.insert(doc, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                done();
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const doc = {
              _id: '1234567890',
              name: { first: 'bob', last: 'jones' },
              phone_number: '1234567890',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: (new Date()).toJSON(), points_earned: 50 },
              ],
            };
            const appUsers = nano.use('app_users');
            appUsers.insert(doc, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              done();
            });
          });
        }
      });
    });

    it('should return status code 200 with body equal to reference when given valid id', (done) => {
      request(app)
        .get('/current_points/1234567890')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.eql({ current_points: 50 });
          return done();
        });
    });

    it('should return status code 404 with body equal to reference when given invalid id', (done) => {
      request(app)
        .get('/current_points/0987654321')
        .expect('Content-Type', /json/)
        .expect(500, done);
    });
  });

  describe('PUT /new_checkin/:id', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const doc1 = {
                _id: '1234567890',
                name: { first: 'bob', last: 'jones' },
                phone_number: '1234567890',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
                ],
              };
              const doc2 = {
                _id: '0987654321',
                name: { first: 'tammy', last: 'jones' },
                phone_number: '0987654321',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: (new Date()).toJSON(), points_earned: 50 },
                ],
              };
              const appUsers = nano.use('app_users');
              appUsers.insert(doc1, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                appUsers.insert(doc2, (insertErr2) => {
                  if (insertErr2) throw new Error(insertErr2);
                  done();
                });
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const doc1 = {
              _id: '1234567890',
              name: { first: 'bob', last: 'jones' },
              phone_number: '1234567890',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
              ],
            };
            const doc2 = {
              _id: '0987654321',
              name: { first: 'tammy', last: 'jones' },
              phone_number: '0987654321',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: (new Date()).toJSON(), points_earned: 50 },
              ],
            };
            const appUsers = nano.use('app_users');
            appUsers.insert(doc1, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              appUsers.insert(doc2, (insertErr2) => {
                if (insertErr2) throw new Error(insertErr2);
                done();
              });
            });
          });
        }
      });
    });

    it('should return status code 200 with checkedin===true when able to check in', (done) => {
      request(app)
        .put('/new_checkin/1234567890')
        .send({})
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.have.keys('checkedin', 'last_checkin');
          expect(res.body.checkedin).to.be.ok();
          return done();
        });
    });

    it('should return status code 200 with checkedin===false when not able to check in', (done) => {
      request(app)
        .put('/new_checkin/0987654321')
        .send({})
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body).to.have.keys('checkedin', 'last_checkin');
          expect(res.body.checkedin).to.not.be.ok();
          return done();
        });
    });
  });

  describe('GET /can_checkin/:id', () => {
    before((done) => {
      nano.db.list((err, body) => {
        if (err) throw new Error(err);
        if (body.includes('app_users')) {
          nano.db.destroy('app_users', (inerr) => {
            if (inerr) throw new Error(inerr);
            nano.db.create('app_users', () => {
              const doc1 = {
                _id: '1234567890',
                name: { first: 'bob', last: 'jones' },
                phone_number: '1234567890',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
                ],
              };
              const doc2 = {
                _id: '0987654321',
                name: { first: 'tammy', last: 'jones' },
                phone_number: '0987654321',
                email_address: 'bob.jones@email.com',
                current_points: 50,
                check_ins: [
                  { timestamp: (new Date()).toJSON(), points_earned: 50 },
                ],
              };
              const appUsers = nano.use('app_users');
              appUsers.insert(doc1, (insertErr) => {
                if (insertErr) throw new Error(insertErr);
                appUsers.insert(doc2, (insertErr2) => {
                  if (insertErr2) throw new Error(insertErr2);
                  done();
                });
              });
            });
          });
        } else {
          nano.db.create('app_users', () => {
            const doc1 = {
              _id: '1234567890',
              name: { first: 'bob', last: 'jones' },
              phone_number: '1234567890',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: '2000-01-01T00:00:00.000Z', points_earned: 50 },
              ],
            };
            const doc2 = {
              _id: '0987654321',
              name: { first: 'tammy', last: 'jones' },
              phone_number: '0987654321',
              email_address: 'bob.jones@email.com',
              current_points: 50,
              check_ins: [
                { timestamp: (new Date()).toJSON(), points_earned: 50 },
              ],
            };
            const appUsers = nano.use('app_users');
            appUsers.insert(doc1, (insertErr) => {
              if (insertErr) throw new Error(insertErr);
              appUsers.insert(doc2, (insertErr2) => {
                if (insertErr2) throw new Error(insertErr2);
                done();
              });
            });
          });
        }
      });
    });

    it('should provide true response if id can check in', (done) => {
      request(app)
        .get('/can_checkin/1234567890')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.can_checkin).to.be.ok();
          return done();
        });
    });

    it('should provide false response if id can not check in', (done) => {
      request(app)
        .get('/can_checkin/0987654321')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.can_checkin).to.not.be.ok();
          return done();
        });
    });
  });
});
