const expect = require('expect.js');

const userValidate = require('../src/user-validator.js');

describe('backend/src/user-validator.js', () => {
  describe('userValidate()', () => {
    it('should succeed on valid input', () => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      expect(userValidate(test)).to.be.ok();
    });

    it('should fail on invalid input', () => {
      const test = {
        _id: '1234567890',
        name: { first: 'bob', last: 'jones' },
        phone_number: '1234567890',
        email_address: 'bob.jones@email.com',
        // current_points: 50,
        check_ins: [
          { timestamp: (new Date()).toJSON(), points_earned: 50 },
        ],
      };

      expect(userValidate(test)).to.not.be.ok();
    });
  });
});
