const appUsers = require('nano')('http://couchdb:5984/app_users');
const userValidate = require('./user-validator.js');

function createNewUser(data) {
  return new Promise((resolve, reject) => {
    if (userValidate(data)) {
      appUsers.insert(data, (err, body) => {
        if (err) return reject(err);
        const output = { ...body, malformed: false };
        return resolve(output);
      });
    } else {
      return resolve({ malformed: true });
    }
  });
}

module.exports = createNewUser;
