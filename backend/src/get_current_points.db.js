const appUsers = require('nano')('http://couchdb:5984/app_users');

function getCurrentPoints(id) {
  return new Promise((resolve, reject) => {
    appUsers.get(id, (err, body) => {
      if (err) return reject(err);
      if (body === undefined) {
        return resolve({ current_points: 0 });
      }
      return resolve({ current_points: body.current_points });
    });
  });
}

module.exports = getCurrentPoints;

