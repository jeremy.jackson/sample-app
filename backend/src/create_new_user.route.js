const express = require('express');
const bodyParser = require('body-parser');
const createNewUser = require('./create_new_user.db.js');

const router = express.Router();

router.use(bodyParser.json());

router.post('/', (req, res) => {
  createNewUser(req.body)
    .then((result) => {
      if (result.malformed) {
        res.status(400).send(result);
      } else {
        res.status(200).send(result);
      }
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;

