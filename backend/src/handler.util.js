/* eslint no-console: "off" */
const nano = require('nano')('http://couchdb:5984');

nano.db.list((err, body) => {
  if (err) throw new Error(err);
  if (body.includes('app_users')) {
    console.log('app_users found. Exiting');
  } else {
    console.log('app_users not found. Creating.');
    nano.db.create('app_users', (inerr) => {
      if (err) throw new Error(inerr);
      console.log('app_users created. Exiting.');
    });
  }
});

