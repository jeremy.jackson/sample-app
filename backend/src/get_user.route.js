const express = require('express');
const getUser = require('./get_user.db.js');

const router = express.Router();

router.get('/:id', (req, res) => {
  getUser(req.params.id)
    .then((exists) => {
      if (exists) {
        res.status(200).json({ exists: 1 });
      } else {
        res.status(404).json({ exists: 0 });
      }
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;
