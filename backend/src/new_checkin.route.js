const express = require('express');
const newCheckin = require('./new_checkin.db.js');

const router = express.Router();

router.put('/:id', (req, res) => {
  newCheckin(req.params.id)
    .then((result) => {
      res.status(200).json(result);
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;

