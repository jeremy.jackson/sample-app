const appUsers = require('nano')('http://couchdb:5984/app_users');

function getUser(id) {
  return new Promise((resolve, reject) => {
    appUsers.list((err, body) => {
      if (err) return reject(err);
      let response = false;
      body.rows.forEach((doc) => {
        if (doc.id === id) response = true;
      });
      return resolve(response);
    });
  });
}

module.exports = getUser;
