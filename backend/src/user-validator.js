const Jsonschema = require('jsonschema').Validator;

const validator = new Jsonschema();

function userValidate(toValidate) {
  const schema = {
    id: '/User',
    type: 'object',
    properties: {
      _id: { type: 'string' },
      name: {
        type: 'object',
        properties: {
          first: { type: 'string' },
          last: { type: 'string' },
        },
        required: [
          'first',
          'last',
        ],
      },
      phone_number: { type: 'string' },
      email_address: { type: 'string' },
      current_points: { type: 'number' },
      check_ins: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            timestamp: { type: 'string' },
            points_earned: { type: 'number' },
          },
          required: [
            'timestamp',
            'points_earned',
          ],
        },
      },
    },
    required: [
      '_id',
      'name',
      'phone_number',
      'email_address',
      'current_points',
      'check_ins',
    ],
  };

  const output = validator.validate(toValidate, schema);
  const isValid = output.errors.length === 0;
  return isValid;
}

module.exports = userValidate;
