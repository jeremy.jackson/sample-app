const express = require('express');
const canCheckin = require('./can_checkin.db.js');

const router = express.Router();

router.get('/:id', (req, res) => {
  canCheckin(req.params.id)
    .then((doc) => {
      if (doc) {
        res.status(200).send(doc);
      } else {
        res.status(404);
      }
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;

