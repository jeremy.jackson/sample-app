function checkFiveMinutes(t1, t2) {
  const check = Math.abs(Date.parse(t1) - Date.parse(t2));
  return check > 300000;
}

module.exports = checkFiveMinutes;
