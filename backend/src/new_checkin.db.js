const appUsers = require('nano')('http://couchdb:5984/app_users');
const checkFiveMinutes = require('./check_five_minutes.util.js');

function newCheckin(id) {
  return new Promise((resolve, reject) => {
    appUsers.get(id, (err, body) => {
      if (err) return reject(err);
      if (checkFiveMinutes((new Date()).toJSON(), body.check_ins[0].timestamp)) {
        const toInsert = { ...body };
        toInsert.check_ins.unshift({ timestamp: (new Date()).toJSON(), points_earned: 20 });
        toInsert.current_points += 20;
        appUsers.insert(toInsert, (insertErr) => {
          if (insertErr) return reject(insertErr);
          return 1;
        });
        return resolve({ checkedin: true, last_checkin: toInsert.check_ins[0].timestamp });
      }
      return resolve({ checkedin: false, last_checkin: body.check_ins[0].timestamp });
    });
  });
}

module.exports = newCheckin;
