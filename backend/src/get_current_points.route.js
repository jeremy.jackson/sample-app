const express = require('express');
const getCurrentPoints = require('./get_current_points.db.js');

const router = express.Router();

router.get('/:id', (req, res) => {
  getCurrentPoints(req.params.id)
    .then((doc) => {
      if (doc) {
        res.status(200).send(doc);
      } else {
        res.status(404);
      }
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;

