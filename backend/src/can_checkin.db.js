const appUsers = require('nano')('http://couchdb:5984/app_users');
const checkFiveMinutes = require('./check_five_minutes.util.js');

function newCheckin(id) {
  return new Promise((resolve, reject) => {
    appUsers.get(id, (err, body) => {
      if (err) return reject(err);
      return resolve({
        can_checkin: checkFiveMinutes((new Date()).toJSON(), body.check_ins[0].timestamp),
      });
    });
  });
}

module.exports = newCheckin;

